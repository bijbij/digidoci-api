# عملیات

به طور کلی عملیات زیر در دیجی‌دکی تعریف شده است:

- create (C)
- remove (R)
- edit (E)
- view (V)

# Permissions

دسترسی‌های زیر در دیجی‌دکی تعریف شده است:

| Entity         | owner             | member            | authorized (registered)     | unanimous (others)     |
|:---------------|:-----------------:|:-----------------:|:---------------------------:|:----------------------:|
| Device         | all               | V                 | V                           | V                      |
| DeviceModel    | all               | V                 | V                           | V                      |
| Color          | all               | V                 | V                           | V                      |
| Network        | all               | V                 | V                           | V                      |
| Problem        | all               | V                 | V                           | V                      |
| Request        | all               | C,R,E,V           | C, V*                       | C                      |
| RequestHistory | V                 | V                 | V*                          | -                      |
| Response       | all               | C,E,V             | V*                          | -                      |

در جدول فوق علامت V* به این معنی است که امکان مشاهده تنها در صورتی که شناسه کاربر در آن عنصر با شناسه کاربر مشاهده کننده یکی باشد امکان پذیر است.